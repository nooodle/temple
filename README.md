Inspired by tsoding's aoc-2021 setup.

## Quick Start

### Dependencies

- [QEMU](https://www.qemu.org/)
- [rsync](https://rsync.samba.org/)

### Configuration

All of the scripts use configuration from `config.sh` script.

### TempleOS Installation

```console
$ ./install.sh <img>.img
# Transition from T(emp): to installed VDI
$ ./sync.sh temple ./<img>.img
```

### Running

```console
$ ./run.sh ./<img>.img
# Boot the C drive 
```

### Mounting the File System

```console
$ ./mount.sh ./<img>.img
$ cd ./mnt/
```

You can run `./mount.sh` script several times, it unmounts the image before trying to mount it again. Also the `run.sh` also unmounts the image before running the VM. You usually never have to unmount it manually, but if you need to you can always do `sudo umount ./mnt/`.

### Sync the Home between the Host and the Temple

The subcommand (`temple` or `host`) basically denotes the target.

#### From Host to Temple

```console
$ ./sync.sh temple ./<img>.img
```

#### From Temple to Host

```console
$ ./sync.sh host ./<img>.img
```
